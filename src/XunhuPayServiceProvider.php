<?php
namespace Xunhu\XunhuPay;

use Illuminate\Support\ServiceProvider;

class XunhuPayServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {

            // Publishing the configuration file.
            $this->publishes([
                __DIR__.'/../config/xunhupay.php' => config_path('xunhupay.php'),
            ], 'xunhupay.config');
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/xunhupay.php', 'xunhupay');

        // Register the service the package provides.
        $this->app->singleton('xunhupay', function ($app) {
            return new XunhuPay;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['xunhupay'];
    }
}